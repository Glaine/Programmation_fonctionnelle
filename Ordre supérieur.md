# Ordre supérieur

## Module List

### Map

Fonction qui applique une fonction à tous les éléments d'une liste, sans changer leur positon. 

```ocaml
map : ('a -> 'b) -> 'a list -> 'b list

double : int -> int
map double l : (int -> int) -> int list -> int list
```

