(* ----------------------------------------------------------------- *)
(* ----------------------------------------------------------------- *)

(* FAIRE d'abord EVALUER : open List *)

(* ----------------------------------------------------------------- *)

(* sauf indication contraire :
   pour traiter les listes N'UTILISER QUE :

     []
     ::
     = []
     hd
     tl
*)

(* NE définir AUCUNE fonction auxiliaire
   ET
   N'utiliser NI fst NI snd
*)

(* ----------------------------------------------------------------- *)

(* (x , l)
   ->
   liste dont les elements successifs sont ceux de l suivis de x
*)

val ajout_en_fin : 'a * 'a list -> 'a list

(* ----------------------------------------------------------------- *)

(* ESSAYER d'abord SANS utiliser ajout_en_fin
   PUIS
   si on ne trouve pas de solution EN l'utilisant
*)

(* l -> liste dont les elements sont ceux de l dans l'ordre inverse *)

val inverse : 'a list -> 'a list

(* ----------------------------------------------------------------- *)

(* s'applique a une liste NON vide *)

(* l -> valeur du dernier element de l *)

val dernier : 'a list -> 'a

(* ----------------------------------------------------------------- *)

(* s'applique a une liste NON vide *)

(* l -> liste dont les elements successifs
        sont ceux de l sauf son dernier element
*)

val sans_dernier : 'a list -> 'a list

(* ----------------------------------------------------------------- *)

(* ATTENTION : N'utiliser NI dernier NI sans_dernier *)

(* s'applique a une liste NON vide *)

(* l -> (dernier (l) , sans-dernier (l)) *)

val dernier_sans_dernier : 'a list -> 'a * 'a list

(* ----------------------------------------------------------------- *)

(* (l1 , l2)
   ->
   liste dont les elements successifs sont ceux de l1
   suivis de ceux de l2
*)

val colle : 'a list * 'a list -> 'a list

(* ----------------------------------------------------------------- *)

(* VERSION 1 : EN utilisant colle
   VERSION 2 : SANS l'utiliser
*)

(* ll
   ->
   liste dont les elements
   sont les elements successifs des listes successives de ll
*)

val colle_toutes : 'a list list -> 'a list

(* ----------------------------------------------------------------- *)

(* ATTENTION  : NE PAS utiliser length (ni longueur) *)

(* ll
   ->
   liste dont les elements successifs
   sont les longueurs des listes successives de ll
*)

val liste_des_longueurs : 'a list list -> int list

(* ----------------------------------------------------------------- *)
(* ----------------------------------------------------------------- *)


