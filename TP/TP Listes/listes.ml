open List;;

(*
let l = [1;2;3;4;5];;
let l2 = [[1;2];[3;4];[5]];;
*)

let rec dernier l =
    if tl(l) = [] then
        hd(l)
    else
        dernier(tl(l))
;;

dernier([1;2;3;4;5]);;


let rec sans_dernier l =
    if tl(l) = [] then
        []
    else
        hd(l)::sans_dernier(tl(l))
;;

sans_dernier([1;2;3;4;5]);;

let rec dernier_sans_dernier l =
    if tl(l) = [] then
        (hd(l), [])
    else
        let 
            (a,b) = dernier_sans_dernier(tl(l))
        in
            (a, hd(l)::b)
;;

dernier_sans_dernier([1;2;3;4;5]);;

let rec ajout_en_fin (e,l) =
    if l = [] then
        [e]
    else
        hd(l)::ajout_en_fin(e,tl(l))
;;

ajout_en_fin(6,[1;2;3;4;5]);;

let rec colle_toutes l =
    if l = [] then
        []
    else
        if hd(l) = [] then
            colle_toutes(tl(l))
        else
            hd(hd(l))::colle_toutes(tl(hd(l))::tl(l))
;;

colle_toutes([[1;2];[3;4];[5]]);;

let rec liste_des_longueurs l =
    if l = [] then
        []
    else
        if hd(l) = [] then
            0::liste_des_longueurs(tl(l))
        else
            let 
                x = liste_des_longueurs(tl(hd(l))::tl(l))
            in
                (1+hd(x))::tl(x)
;;

liste_des_longueurs([[1;2];[3;4];[5]]);;

let rec inverse l = 
    if l = [] then
        []
    else
        ajout_en_fin(hd(l), inverse(tl(l)))
;;

inverse([1;2;3;4;5]);;

let rec colle(l1,l2) =
    if l1 = [] then
        l2
    else
        hd(l1)::colle(tl(l1),l2)
;;

colle([1;2;3],[4;5;6]);;

let rec mymap f l =
    if l = [] then
        []
    else
        f(hd(l))::mymap f (tl(l))
;;

let double x =
    x*2
;;
mymap double [1;2;3];;