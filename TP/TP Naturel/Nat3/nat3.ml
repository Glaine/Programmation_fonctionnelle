#use "nat_3.ml";;

let rec naturel_of_int x =
    if x = 0 then
        zero()
    else if x mod 2 = 0 then
        d0(naturel_of_int(x/2))
    else
        d1(naturel_of_int(x/2))
;;

naturel_of_int(2);;

let rec int_of_naturel x =
    if x = zero() then
        0
    else if est_pair_et_non_nul(x) then
        2*(int_of_naturel(inv_d0(x)))
    else
        2*(int_of_naturel(inv_d1(x)))+1
;;

int_of_naturel(naturel_of_int(4));;
int_of_naturel(naturel_of_int(3));;

let rec est_pair x =
    if x = zero() || est_pair_et_non_nul(x) then
        true
    else
        false
;;

est_pair(naturel_of_int(0));;
est_pair(naturel_of_int(1));;
est_pair(naturel_of_int(2));;

let rec plus(x,y) =
    if y = zero() then
        x
    else if est_pair_et_non_nul(y) then
        plus(d0(x), inv_d0(y))
    else
        plus(d1(x), inv_d1(y))
;;

int_of_naturel(plus(naturel_of_int(4), naturel_of_int(2)));;
int_of_naturel(plus(naturel_of_int(4), naturel_of_int(3)));;