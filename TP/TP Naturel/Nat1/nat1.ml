#use "nat_1.ml";;


let rec naturel_of_int (x) =
    if est_zero(x) then 
        0
    else
        1+naturel_of_int(pre(x))
;;

let rec int_of_naturel (x) = 
    if x = 0 then
        zero()
    else
    suc(int_of_naturel(x-1))
;;

let rec est_pair (x) =
    if est_zero(x) then
        true
    else if x = suc(zero()) then
        false
    else
        est_pair(pre(pre(x)))
;;

est_pair(int_of_naturel(4));;
est_pair(int_of_naturel(7));;

let rec plus (x,y) =
    if est_zero(y) then
        x
    else if est_zero(x) then
        y
    else
        plus(suc(x), pre(y))
;;

naturel_of_int(plus(int_of_naturel(4), int_of_naturel(3)));;

let rec fois (x, y) =
    if est_zero(x) || est_zero(y) then
        zero()
    else if y = suc(zero()) then
        x
    else
        plus(fois(x, pre(y)), x)
;;

naturel_of_int(fois(int_of_naturel(3), int_of_naturel(3)));;

let rec supegal (x,y) =
    if est_zero(x) && not(est_zero(y)) then
        false
    else if (not(est_zero(x)) && est_zero(y)) || 
            (est_zero(x) && est_zero(y)) then
        true
    else
        supegal(pre(x), pre(y))
;;

supegal(int_of_naturel(4), int_of_naturel(3));;
supegal(int_of_naturel(3), int_of_naturel(3));;
supegal(int_of_naturel(3), int_of_naturel(4));;

let rec moins (x,y) =
    if est_zero(y) then
        x
    else
        moins(pre(x), pre(y))
;;

naturel_of_int(moins(int_of_naturel(5), int_of_naturel(3)));;

let rec quotient_et_reste (x,y) =
    if not(supegal(x,y)) then
        (zero(), x)
    else
        let (a,b) = quotient_et_reste(moins(x, y), y)
        in (suc(a), b)
;;

let (a,b) = quotient_et_reste(int_of_naturel(20),int_of_naturel(4))
in
(naturel_of_int(a), naturel_of_int(b))
;;
let (a,b) = quotient_et_reste(int_of_naturel(4),int_of_naturel(2))
in
(naturel_of_int (a), naturel_of_int(b))
;;


let rec est_puissance_de_deux (x) =
    if x = suc(suc(zero())) then
        true
    else if x = zero() then
        true
    else if x = suc(zero()) then
        true
    else
        let 
            (a,b) = quotient_et_reste(x, suc(suc(zero())))
        in
            if not(est_zero(b)) then
                false
            else
                est_puissance_de_deux(a)
;;

(est_puissance_de_deux(int_of_naturel(4)));;
(est_puissance_de_deux(int_of_naturel(5)));;
(est_puissance_de_deux(int_of_naturel(1)));;

let rec log_base_deux(x) =
    if x = suc(zero()) then
        zero()
    else
        let
            (a,b) = quotient_et_reste(x, suc(suc(zero())))
        in
            suc(log_base_deux(a))
;;

naturel_of_int(log_base_deux(int_of_naturel(16)));;