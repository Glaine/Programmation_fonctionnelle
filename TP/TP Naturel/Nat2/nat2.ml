#use "nat_2.ml";;

let rec naturel_of_int(x) =
    if x = 0 then
        zero()
    else if x = 1 then
        un()
    else
        sucsuc(naturel_of_int(x-2))
;;

naturel_of_int(1);;
naturel_of_int(3);;

let rec int_of_naturel(x) =
    if x = zero() then
        0
    else if x = un() then
        1
    else
        2+int_of_naturel(prepre(x))
;;

int_of_naturel(sucsuc(un()));;
int_of_naturel(sucsuc(zero()));;

let rec est_pair(x) =
    if x = un() then
        false
    else if x = sucsuc(zero()) then
        true
    else
        est_pair(prepre(x))
;;

(est_pair(naturel_of_int(5)));;
(est_pair(naturel_of_int(6)));;

let rec plus_un (x) =
    if x = zero() then
        un()
    else if x = un() then
        sucsuc(zero())
    else
        sucsuc(plus_un(prepre(x)))
;;

int_of_naturel(plus_un(naturel_of_int(5)));;

let rec moins_un(x) =
    if x = un() then
        zero()
    else if x = sucsuc(zero()) then
        un()
    else
        sucsuc(moins_un(prepre(x)))
;;

int_of_naturel(moins_un(naturel_of_int(5)));;

let rec plus_inter(x, y) =
    if y = zero() then
        x
    else if x = zero() then
        y
    else if y = un() then
        plus_un(x)
    else 
        plus_inter(sucsuc(x), prepre(y))
;;

int_of_naturel(plus_inter(naturel_of_int(0), naturel_of_int(1)));;
int_of_naturel(plus_inter(naturel_of_int(5), naturel_of_int(3)));;

let rec plus(x, y) =
    if y = zero() then
        x
    else if y = un() then
        if x = zero() then
            un()
        else if x = un() then
            sucsuc(zero())
        else
            plus(y,x)
    else
        sucsuc(plus(x,prepre(y)))
;;

int_of_naturel(plus(naturel_of_int(0), naturel_of_int(1)));;
int_of_naturel(plus(naturel_of_int(5), naturel_of_int(3)));;


let rec fois(x,y) =
    if y = zero() || x = zero() then
        zero()
    else if y = un() then
        x
    else
        plus_inter(fois(x, moins_un(y)), x)
;;

int_of_naturel(fois(naturel_of_int(1), naturel_of_int(4)));;
int_of_naturel(fois(naturel_of_int(3), naturel_of_int(4)));;

let rec supegal (x,y) =
    if (est_zero(x) && est_zero(y)) || 
       (not(est_zero(x)) && est_zero(y)) then
            true
    else if 
        est_un(x) && est_un(y) || 
        (not(est_un(x)) && not(est_zero(x)) && (est_zero(y) || est_un(y))) then
            true
    else if est_zero(x) && not(est_zero(y)) then
        false
    else if est_un(x) && not(est_zero(y) && not(est_un(y))) then
        false
    else
        supegal(prepre(x), prepre(y))
;;

supegal(naturel_of_int(1), naturel_of_int(3));; (*false*)
supegal(naturel_of_int(3), naturel_of_int(3));; (*true*)
supegal(naturel_of_int(3), naturel_of_int(2));; (*true*)
supegal(naturel_of_int(5), naturel_of_int(3));; (*true*)

let rec moins(x,y) =
    if y = zero() then
        x
    else if y = un() then
        if x = un() then
            zero()
        else if x = sucsuc(zero()) then
            un()
        else
            sucsuc(moins(prepre(x), y))
    else
        moins(prepre(x), prepre(y))
;;

int_of_naturel(moins(naturel_of_int(5), naturel_of_int(1)));;
int_of_naturel(moins(naturel_of_int(5), naturel_of_int(3)));;

let rec quotient_et_reste(x,y) =
    if not(supegal(x, y)) then
        (zero(), x)
    else if not(supegal(moins(x,y), y)) then
        (un(), moins(x,y))
    else
        let (a,b) = quotient_et_reste(moins(moins(x,y), y), y)
        in (sucsuc(a), b)
;;

let (a,b) = quotient_et_reste(naturel_of_int(20),naturel_of_int(4))
in
(int_of_naturel(a), int_of_naturel(b))
;;

let (a,b) = quotient_et_reste(naturel_of_int(4),naturel_of_int(2))
in
(int_of_naturel(a), int_of_naturel(b))
;;


let rec est_puissance_de_deux(x) =
    if x = sucsuc(zero()) then
        true
    else if x = zero() then
        true
    else if x = un() then
        true
    else 
        let
            (a,b) = quotient_et_reste(x, sucsuc(zero()))
        in  
            if not(est_zero(b)) then
                false
            else
                est_puissance_de_deux(a)
;;

(est_puissance_de_deux(naturel_of_int(4)));;
(est_puissance_de_deux(naturel_of_int(5)));;
(est_puissance_de_deux(naturel_of_int(1)));;

let rec log_base_deux(x) =
    if x = un() then
        zero()
    else
        let
            (a,b) = quotient_et_reste(x, sucsuc(zero()))
        in
            plus_un(log_base_deux(a))
;;

int_of_naturel(log_base_deux(naturel_of_int(16)));;