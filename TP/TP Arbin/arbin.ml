type 'a arbin = V | E of 'a * 'a arbin *'a arbin

exception Arbre_vide

let arbin_vide () = V

let embranche_bin (x, a, a') = E (x, a, a')

let est_arbin_vide a = match a with V -> true | _ -> false

let rac a = match a with  E (x, _, _) -> x | _ -> raise Arbre_vide

let sag a = match a with  E (_, a', _) -> a' | _ -> raise Arbre_vide

let sad a = match a with  E (_, _, a') -> a' | _ -> raise Arbre_vide


