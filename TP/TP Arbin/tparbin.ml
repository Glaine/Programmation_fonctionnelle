type 'a arbin = V | E of 'a * 'a arbin *'a arbin

exception Arbre_vide

let arbin_vide () = V

let embranche_bin (x, a, a') = E (x, a, a')

let est_arbin_vide a = match a with V -> true | _ -> false

let rac a = match a with  E (x, _, _) -> x | _ -> raise Arbre_vide

let sag a = match a with  E (_, a', _) -> a' | _ -> raise Arbre_vide

let sad a = match a with  E (_, _, a') -> a' | _ -> raise Arbre_vide

let ab1 = embranche_bin (1,
            embranche_bin(2,
                arbin_vide(), arbin_vide()),
            embranche_bin(3,
                arbin_vide(), arbin_vide()))
;;

let ab2 = embranche_bin(3,arbin_vide(),arbin_vide());;

let ab3 = embranche_bin (1,
            embranche_bin(2,
                embranche_bin(3,
                    arbin_vide(), arbin_vide()),
                arbin_vide()),
            arbin_vide())
;;

let ab4 = embranche_bin (4,
            embranche_bin(5,
                embranche_bin(6,
                    arbin_vide(), arbin_vide()),
                arbin_vide()),
            arbin_vide())
;;

let rec nb_noeuds_arbin a =
    if est_arbin_vide(a) then
        0
    else 
        1 + nb_noeuds_arbin(sag(a)) + nb_noeuds_arbin(sad(a))
;;

nb_noeuds_arbin(ab1);;

let rec nb_feuilles_arbin a =
    if est_arbin_vide(a) then
        0
    else if est_arbin_vide(sag(a)) && est_arbin_vide(sad(a)) then
        1 + nb_feuilles_arbin(sag(a)) + nb_feuilles_arbin(sad(a))
    else
        0 + nb_feuilles_arbin(sag(a)) + nb_feuilles_arbin(sad(a))
;;

nb_feuilles_arbin(ab1);;

let rec est_membre_arbin(x,a) =
    if not(est_arbin_vide(a)) then
        if rac(a) = x then
            true
        else 
            false || est_membre_arbin(x,sag(a)) || est_membre_arbin(x,sad(a))
    else
        false
;;

est_membre_arbin(5,ab1);;
est_membre_arbin(3,ab1);;
est_membre_arbin(3,ab2);;

let rec hauteur_arbin a =
    if est_arbin_vide(a) then
        0
    else
        max (1+hauteur_arbin(sag(a)))  (1+hauteur_arbin(sad(a)))
;;

hauteur_arbin(ab1);;
hauteur_arbin(ab2);;
hauteur_arbin(ab3);;

let rec arbin_des_doubles a =
    if not(est_arbin_vide(a)) then
        embranche_bin(rac(a)*2,arbin_des_doubles(sag(a)), arbin_des_doubles(sad(a)))
    else
        arbin_vide()
;;

arbin_des_doubles(ab1);;
arbin_des_doubles(ab2);;
arbin_des_doubles(ab3);;

let rec arbin_des_hauteurs(a) =
    if not(est_arbin_vide(a)) then
        embranche_bin(hauteur_arbin(rac(a)), arbin_des_hauteurs(sag(a)), arbin_des_hauteurs(sad(a)))
    else
        arbin_vide()
;;

let rec abab1 = embranche_bin(ab1,
                    embranche_bin(ab2,
                        arbin_vide(), arbin_vide()),
                    embranche_bin(ab3,
                        arbin_vide(), arbin_vide()))
;;

arbin_des_hauteurs(abab1);;

let rec rassemble_arbin(a,b) =
    if not(est_arbin_vide(a)) then
        embranche_bin((rac(a),rac(b)),
            rassemble_arbin(sag(a),sag(b)), rassemble_arbin(sad(a),sad(b)))
    else
        arbin_vide()
;;

rassemble_arbin(ab3,ab4);;

let rec separe_bin(a) =
    if not(est_arbin_vide(a)) then
        let (x,y) = rac(a) in
        let (lg,ld) = separe_bin(sag(a)) in
        let (rg,rd) = separe_bin(sad(a)) in 
            ( embranche_bin(x, lg, rg ) , embranche_bin(y, ld, rd ))
    else
        (arbin_vide(), arbin_vide())
;;

separe_bin(rassemble_arbin(ab3,ab4));;      

let arbre_feuille a =
    embranche_bin(a, arbin_vide(), arbin_vide())
;;