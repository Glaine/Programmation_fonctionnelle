type 'a arbre = ArbreFeuille of 'a | Embranche of 'a * 'a arbre list
val arbre_feuille : 'a -> 'a arbre
val embranche : 'a * 'a arbre list -> 'a arbre
val est_arbre_feuille : 'a arbre -> bool
val racine : 'a arbre -> 'a
val enfants : 'a arbre -> 'a arbre list
