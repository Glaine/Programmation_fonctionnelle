open List;;

type 'a arbre = F of 'a | E of 'a * 'a arbre list

exception TG

let arbre_feuille (a) = F (a)

let embranche (v, l) = E (v, l)

let est_arbre_feuille a = match a with
    F(x) -> true
    | _ -> false

let racine a = match a with 
    E(x,_) -> x
    | F(x) -> x

let enfants a = match a with
    E(_,l)->l
    | _ -> raise TG


let rec arbre_des_doubles a =
    if est_arbre_feuille(a) then
        arbre_feuille(racine(a)*2)
    else
        embranche(racine(a)*2, map arbre_des_doubles (enfants(a)))
;;

let rec pas_vraiment_map_car_ca_fait_map_mais_seulement_en_doublant l =
   if est_arbre_feuille(hd(l)) then
        [arbre_feuille(racine(hd(l))*2)]
    else
        arbre_feuille(racine(hd(l))*2)::pas_vraiment_map_car_ca_fait_map_mais_seulement_en_doublant(tl(l))
;;

let rec arbre_des_doubles_bis a =
    if est_arbre_feuille(a) then
        arbre_feuille(racine(a)*2)
    else
        embranche(racine(a)*2, pas_vraiment_map_car_ca_fait_map_mais_seulement_en_doublant(enfants(a)))
;;


let bob = embranche(3, 
	[embranche(7,
		[arbre_feuille(1);arbre_feuille(2)]);
	arbre_feuille(5);
	embranche(6,
		[arbre_feuille(4)])
	]
)
;;

arbre_des_doubles(bob);;

arbre_des_doubles_bis(bob);;