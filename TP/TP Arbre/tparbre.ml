open List;;

type 'a arbre = ArbreFeuille of 'a | Embranche of 'a * 'a arbre list
val arbre_feuille : 'a -> 'a arbre
val embranche : 'a * 'a arbre list -> 'a arbre
val est_arbre_feuille : 'a arbre -> bool
val racine : 'a arbre -> 'a
val enfants : 'a arbre -> 'a arbre list

type 'a arbre = ArbreFeuille of 'a | Embranche of 'a * ('a arbre) list ;;

let arbre_feuille = function x -> ArbreFeuille (x) ;;

let embranche = function 
  x, larb -> if larb = []
               then failwith ("embranche : la liste-parametre ne doit pas etre vide")
               else Embranche (x, larb) ;;

let est_arbre_feuille = function ArbreFeuille (_) -> true | Embranche (_) -> false ;;

let racine = function ArbreFeuille (x) -> x | Embranche (x, _) -> x ;;

let enfants = function
  Embranche (_, larb) -> larb |
  ArbreFeuille (_) -> failwith ("enfants : ne s'applique pas a un arbre-feuille") ;;