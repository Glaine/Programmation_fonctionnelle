# Inférence de type

## Exercice 1

```ocaml
5*4*3*2*1;;
```

\- : int = 30

```ocaml
true;;
```

\- : bool = true

```ocaml
if 2>0 then
	"true"
else
	"false"
;;
```

\- : string = "true"

```ocaml
if 2>0 then
	true
else
	false
;;
```

\- : bool = true 

```ocaml
if 2>0 then
	"oui"
else
	"non"
;;
```

\- : string = "oui"

```ocaml
if 2>0 then
	oui
else 
	non
;;
```

Error : unbound value oui

```ocaml
if 2>0 then
	true
else
	"false"
;;
```

Error : This expression has type string but an expression was expected of type bool

```ocaml
3.14 +. 2.;;
```

\- : float = 5.1400000000000006

Décalage du à la norme IEEE.

```ocaml
3.14 +. 2;;
```

Error : This expression has type int but an expression was expected of type float

```ocaml
3.14 + 2.0;;
```

Error : This expression has type int but an expression was expected of type float

```ocaml
3.14 > 2.;;
```

\- : bool = true

```ocaml
3.14 > 2;;
```

Erreur : This expression has type int but an expression was expected of type float

```ocaml
int_of_float (3.14) > 2;;
```

\- : bool = true

```ocaml
3.14 > float_of_int(2);;
```

\- : bool = true

```ocaml
3 > 2;;
```

\- : bool = true

```ocaml
"trois" > "deux";;
```

\- : bool = true

```ocaml
"deux" > "un";;
```

\- : bool = false

Comparaison lexicographique

```ocaml
5 / 2;;
```

\- : int = 2

Division entière

```ocaml
5. /. 2.;;
```

\- : float = 2.5

```ocaml
sqrt(4.);;
```

\- : float = 2.

```ocaml
sqrt(2);;
```

Error : This expression has type int but an expression was expected of type float

```ocaml
sqrt;;
```

\- : 'float -> 'float = <fun>

```ocaml
exp(1.);;
```

\- : float = 2.7182818284590451

```ocaml
log( exp ( 1.));;
```

\- float = 1.

```ocaml
5 / 0;;
```

Exception : Division_by_zero.

```ocaml
if true then
	5 / 0 
else
	3
;;
```

Exception : Division_by_zero.

```ocaml
if true then
	3
else
	5 / 0
;;
```

\- : int = 3

```ocaml
if false then
	5 / 0
else
	3
;;
```

\- : int = 3

```ocaml
if false then 
	3
else
	5 / 0 
;;
```

Error : Division_by_zero.

```ocaml
(2 / 0 = 5) && true;;
```

Exception : Division_by_zero.

```ocaml
true && (2 / 0 = 5);;
```

Exception : Division_by_zero.

```ocaml
(2 / 0 = 5) && false;; 
```

Exception : Division_by_zero.

```ocaml
false && (2 / 0 = 5);;
```

\- : bool = false

```ocaml
(2 / 0 = 5) || true;;
```

Exception : Division_by_zero.

```ocaml
true || (2 / 0 = 5);;
```

\- : bool = true

```ocaml
(2 / 0 = 5) || false;;
```

Exception : Division_by_zero.

```ocaml
false || (2 / 0 = 5);;
```

\- : bool = false

```ocaml
"s" ^ "i";;
```

\- : string = "si"

```ocaml
's' ^ "i";;
```

Error : This expression has type char but an expression was expected of type string

```ocaml
's' ^ 'i';;
```

Error : This expression has type char but expression was expected of type string

```ocaml
"s" ^ i;;
```

Error : Unbound value i

```ocaml
si;;
```

Error : Unbound value si

```ocaml
"if";;
```

\- : string  = "si"

```ocaml
function x -> x > 0;;
```

\- : int -> bool =<fun>

```ocaml
(function x -> x > 0) (12)
```

\- : bool = true

```ocaml
function x -> 2 * x;;
```

\- : int -> int = <fun>

```ocaml
(function x -> 2 * x) (7);;
```

\- : int = 14

```ocaml
function (x,y) -> (x + y, x - y);;
```

`-: int * int -> int * int = <fun>`

```ocaml
function x -> x / 0;;
```

\- : int -> int = <fun>

```ocaml
(function x -> x / 0) (12);;
```

Error : Division_by_zero.

```ocaml
(function x -> x) (5);;
```

\- : int = 5

```ocaml
(function x -> x) ("oui");;
```

\- : string = "oui"

```ocaml
function x -> x;; 
```

\- : 'a -> 'a

```ocaml
(function x -> 1)(5);;
```

\- : int = 1

```ocaml
(function x -> 1)(5.);;
```

\- : int  = 1

```ocaml
function x -> 1;;
```

\- : int = 1

```ocaml
(function (x,y) -> (y,x)) (1,2);;
```

`- : int * int = (2,1) `

```ocaml
(function (x,y)->(y,x))(1,"oui");;
```

`- : string * int = ("oui", 1)`

```ocaml
(function (x,y) -> (y,x))("oui", "oui");;
```

`- : string * string = ("oui", "oui")`

```ocaml
function (x,y) -> (y,x);;
```

`- : 'a * 'b -> 'b * 'a = <fun>`

```ocaml
(function x -> (x,y))(1);;
```

Error : Unbound value y

```ocaml
let aaa = 2 and bbb = 3 * aaa in aaa + bbb;;
```

Error : Unbound value aaa

```ocaml
let aaa = 2 in let bbb = 3 * aaa in aaa + bbb;;
```

\- : int = 8

```ocaml
let aaa = 2 in let bbb = 3 * aAa in aaa + bbb;;
```

Error : Unbound value aAa

## Exercice 2

```ocaml
let absolu = function x -> 
	if x >=0 then
    	x
    else
    	-x
 ;;
```

val absolu : int -> int = <fun>

```ocaml
absolu(absolu(-12));;
```

\- : int = 12

```ocaml
let comp = function(x, y, z) -> (x <= y, y<= z);;
```

comp : 'a * 'a * 'a -> bool * bool = <fun>

```ocaml
(comp(3,2,4), comp(2,3,4));;
```

\- : (bool * bool) * (bool * bool) = ((false, true), (true, true))

```ocaml
comp("jaune", "rouge", "vert");;
```

\- : bool * bool = (true, true)

```ocaml
let approx = function(a,b) -> let (b1, b2) = comp(a-1, b, a+1) in b1 && b2;;
```

val approx : int * int -> bool = <fun>

```ocaml
approx(12,13);;
```

\- : bool = true

```ocaml
approx("mauve", "violet");;
```

Error : This expression has type string but an expression was expected of type int

```ocaml
let ff = function x -> x + 1 in ff;;
```

\- : int -> int = <fun>

```ocaml
ff(5);;
```

Error : Unbound value ff

```ocaml
let ff = function x -> x + 1. ;;
```

Error : This expression has type float but an expression was expected of type int

```ocaml
ff(5);;
```

Error : Unbound value ff

```ocaml
let ff = function x -> x +. 1.;;
```

\- : float -> float

```ocaml
ff(5);;
```

Error : Unbound value ff

```ocaml
(let ff = function x -> x + 1 in ff) (5);;
```

\- : int = 6

```ocaml
ff;;
```

Error : Unbound value ff

```ocaml
let fff = function b -> b;;
```

val fff : 'a -> 'a = <fun>

```ocaml
fff(3);;
```

\- : int = 3

```ocaml
fff("oui");;
```

\- : string = "oui"

```ocaml
let ffff c = c;;
```

val ffff : 'a -> 'a = <fun>

```ocaml
ffff(3);;
```

\- : int = 3

```ocaml
ffff("oui");;
```

\- : string = "oui"

### Exercice 3

int -> int 

```ocaml
let a(x) = x + 1;;
```

int * int -> bool

```ocaml
let a(x, y) = x+y = y+x;;
```

string -> bool

```ocaml
let a(x) = x = "oui";;
```

float * float -> string

```ocaml
let a(x,y) =
	if x +. y = 3.2 then
		"oui"
	else
		"non"
;;
```

bool * bool -> bool

```ocaml
let a(x,y) = x && y;;
```

int -> int * bool

```ocaml
let a(x) =
	if x > 0 then
		(x, true)
	else
		(-x, false)
;;
```

### Exercice 4

f (f (g (2, true), 4))

```ocaml
let g(x, y) =
	if x = 2 && y = true then
		4
	else 2
;;

let f(x, y) = (x+2, y+3);;
```

### Exercice 5

