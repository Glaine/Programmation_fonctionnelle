# Type Naturel

```ocaml
naturel (*représente l'ensemble des entiers naturels*)
```

-------

## Constructeurs

```ocaml
zero : unit -> naturel			(*constructeur de base*)
suc : naturel -> naturel		(*constructeur inductif*)

unit = () (*c'est la valeur de unit*)
```

______

## Déconstructeur

```ocaml
(*s'applique aux valeurs différentes de zero()*)
pre : naturel -> naturel
```

______

## Prédicat

```ocaml
est_zero : naturel -> bool	(*prédicat n'est pas une appélation standard*)
```

_____

_____

## Fonctions

### est_pair

```ocaml
est-pair : naturel -> bool

(*version terminale*)
let rec est_pair (x) =
	if est_zero(x) then
		true
	else if x = suc(zero()) then
		false
	else
		est_pair(pre(pre(x)))
;;

(*version non terminale*)
let rec est_pair(x) =
	if est_zero(x) then
		true
	else
		not(est_pair(pre(x)))
;;
```

### plus

```ocaml
plus : naturel * naturel -> naturel

let rec plus (x,y) =
	if est_zero(x) then
		y
	else if est_zero(y) then
		x
	else
		plus(suc(x), pre(y))
;;
```

### fois

```ocaml
fois : naturel * naturel -> naturel

let rec fois(x,y) =
	if est_zero(y) || est_zero(x) then
		zero()
	else if x = suc(zero()) then
		y
	else if y = suc(zero()) then
		x
	else
		plus(fois(x, pre(y)), x)
;;
```



### supegal

```ocaml
supegal : naturel * naturel -> bool

let rec supegal(x,y) =
	if est_zero(y) && not(est_zero(x)) then
		true
	else if est_zero(y) && est_zero(x) then
		true
	else if not(est_zero(x)) && est_zero(y) then
		false
	else
		supegal(pre(x), pre(y))
;;
```

