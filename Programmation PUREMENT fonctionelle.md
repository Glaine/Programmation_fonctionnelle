# Introduction

## Inférence de type

```ocaml
function x -> x mod 2 = 0;;
- : int -> bool = <fun>

function x -> if x mod 2 = 0 then true else false;;
- : int -> bool = <fun>

(function x -> if x mod 2 = 0 then true else false) (3+5);;
- : bool = true

2+6;;
- : int = 8

(* 
fun et fonction sont équivalents
function et -> sont redondants
*)

fun x y z -> x (y z);;
- : ('a -> 'b) -> ('c -> 'a) -> 'c -> 'b = <fun>

exp;;
- : float -> float = <fun>
(*
exp
*)

function a -> (function x -> a * x);;
- : int -> int -> int = <fun>
(*
une valeur fonctionelle est de l'ordre supérieur si elle a une flèche
*)
(function a -> (function x -> a * x)) 5;;
- : int -> int = <fun>
(*multiplie a par 5*)
((function a -> (function x -> a * x)) 5) 8;;
- : int = 40
(*multiplie 8 par 5*)
```

Ocaml transforme un code en arbre syntaxique. Un programme est une suite de caractères, lorque qu'il est compilé, le compilateur fait :

	1. L'analyse lexicale : vérifie les érreurs lexicale et transforme le code en une liste de tokens
	2. L'analyseur syntaxique : il transforme la liste de tokens en arbre de tokens de manière récursive.

L'inférence de type abstrait l'information en analysant les opérateur, des opérateurs il en récupère les types.