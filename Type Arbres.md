# Type Arbre

## Constructeurs

```ocaml
arbre_feuille : 'a -> 'a arbre			(*arbre avec un seul noeud*)
embranche : 'a * 'a arbre list -> 'a arbre	(*la liste doit être non vide*)
```

```ocaml
embranche(3, 
	[embranche(7,
		[arbre_feuille(1);arbre_feuille(2)]);
	arbre_feuille(5);
	embranche(6,
		[arbre_feuille(4)])
	]
)
;;
```

## Prédicat

```ocaml
est_arbre_feuille : 'a arbre -> bool
racine : 'a arbre -> 'a
(*s'applique à un arbre non feuille*)
enfants : 'a arbre -> 'a arbre list
(*la liste résultat est donc non vide*)
```

## Fonctions

```ocaml
arbre_des_doubles : int arbre -> int arbre
let rec arbre_des_doubles a =
	if est_arbre_feuille(a) then
		arbre_feuille(racine(a)*2)
	else
		embranche(racine(a)*2, map arbre_des_doubles (enfants(a)))
```

