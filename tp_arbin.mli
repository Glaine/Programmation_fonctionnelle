(* ----------------------------------------------------------------- *)
(* ----------------------------------------------------------------- *)

(* FAIRE d'abord EVALUER :

#load "arbin.cmo"

PUIS

open Arbin

*)

(* ----------------------------------------------------------------- *)

(* NE définir AUCUNE fonction auxiliaire
   ET
   N'utiliser NI fst NI snd
*)

(* ----------------------------------------------------------------- *)
(* ----------------------------------------------------------------- *)

(* a -> nombre de noeuds de a *)
 
val nb_noeuds_arbin : 'a arbin -> int

(* ----------------------------------------------------------------- *)

(* a -> nombre de feuilles de a *)
 
val nb_feuilles_arbin : 'a arbin -> int

(* ----------------------------------------------------------------- *)

(* (x , a)
   ->
   true si un noeud de a au moins a pour valeur associee x,
   false sinon
*)
 
val est_membre_arbin : 'a * 'a arbin -> bool

(* ----------------------------------------------------------------- *)

(* vu en CM *)

(* a -> hauteur de a (en nombre de niveaux (de noeuds)) *)
 
val hauteur_arbin : 'a arbin -> int

(* ----------------------------------------------------------------- *)

(* a
   ->
   arbre ayant meme forme que a
   et
   dans lequel la valeur associee a un noeud
   est le double de celle associee au noeud correspondant de a
*)
 
val arbin_des_doubles : int arbin -> int arbin

(* ----------------------------------------------------------------- *)

(* UTILISER hauteur_arbin *)

(* a
   ->
   arbre ayant meme forme que a
   et
   dans lequel la valeur associee a un noeud
   est la hauteur de celle associee (celle-ci est un arbre)
   au noeud correspondant de a
*)
 
val arbin_des_hauteurs : 'a arbin arbin -> int arbin

(* ----------------------------------------------------------------- *)

(* ON SUPPOSE que (a1 , a2) sont des arbres DE MEME FORME *)

(* (a1 , a2)
   ->
   arbre ayant meme forme que a1 (et donc que a2)
   et
   dans lequel la valeur associee a un noeud
   est le couple (x1 , x2)
   ou x1 (resp. x2) est la valeur associee
   au noeud correspondant de a1 (resp. a2)
*)
 
val rassemble_arbin : 'a arbin * 'b arbin -> ('a * 'b) arbin

(* ----------------------------------------------------------------- *)

(* fonction reciproque (ou inverse) de rassemble_arbin *)

val separe_arbin : ('a * 'b) arbin -> 'a arbin * 'b arbin

(* ----------------------------------------------------------------- *)
(* ----------------------------------------------------------------- *)




